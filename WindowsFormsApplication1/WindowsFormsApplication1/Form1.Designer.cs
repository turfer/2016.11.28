﻿namespace WindowsFormsApplication1
{
    partial class Form1
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.Ekranas = new System.Windows.Forms.TextBox();
            this.Skaicius1 = new System.Windows.Forms.Button();
            this.Skaicius2 = new System.Windows.Forms.Button();
            this.Skaicius3 = new System.Windows.Forms.Button();
            this.Skaicius5 = new System.Windows.Forms.Button();
            this.Skaicius4 = new System.Windows.Forms.Button();
            this.Skaicius6 = new System.Windows.Forms.Button();
            this.Skaicius7 = new System.Windows.Forms.Button();
            this.Skaicius8 = new System.Windows.Forms.Button();
            this.Skaicius9 = new System.Windows.Forms.Button();
            this.zenklas4 = new System.Windows.Forms.Button();
            this.Skaicius0 = new System.Windows.Forms.Button();
            this.zenklas5 = new System.Windows.Forms.Button();
            this.zenklas1 = new System.Windows.Forms.Button();
            this.zenklas2 = new System.Windows.Forms.Button();
            this.zenklas3 = new System.Windows.Forms.Button();
            this.button1 = new System.Windows.Forms.Button();
            this.button2 = new System.Windows.Forms.Button();
            this.button3 = new System.Windows.Forms.Button();
            this.taskas = new System.Windows.Forms.Button();
            this.SuspendLayout();
            // 
            // Ekranas
            // 
            this.Ekranas.Enabled = false;
            this.Ekranas.Font = new System.Drawing.Font("Microsoft Sans Serif", 20.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.Ekranas.Location = new System.Drawing.Point(50, 32);
            this.Ekranas.Name = "Ekranas";
            this.Ekranas.Size = new System.Drawing.Size(200, 38);
            this.Ekranas.TabIndex = 0;
            this.Ekranas.TextAlign = System.Windows.Forms.HorizontalAlignment.Right;
            this.Ekranas.TextChanged += new System.EventHandler(this.textBox1_TextChanged);
            // 
            // Skaicius1
            // 
            this.Skaicius1.Location = new System.Drawing.Point(50, 74);
            this.Skaicius1.Name = "Skaicius1";
            this.Skaicius1.Size = new System.Drawing.Size(39, 23);
            this.Skaicius1.TabIndex = 1;
            this.Skaicius1.Text = "1";
            this.Skaicius1.UseVisualStyleBackColor = true;
            this.Skaicius1.Click += new System.EventHandler(this.button1_Click);
            // 
            // Skaicius2
            // 
            this.Skaicius2.Location = new System.Drawing.Point(118, 74);
            this.Skaicius2.Name = "Skaicius2";
            this.Skaicius2.Size = new System.Drawing.Size(39, 23);
            this.Skaicius2.TabIndex = 2;
            this.Skaicius2.Text = "2";
            this.Skaicius2.UseVisualStyleBackColor = true;
            this.Skaicius2.Click += new System.EventHandler(this.button2_Click);
            // 
            // Skaicius3
            // 
            this.Skaicius3.Location = new System.Drawing.Point(189, 74);
            this.Skaicius3.Name = "Skaicius3";
            this.Skaicius3.Size = new System.Drawing.Size(39, 23);
            this.Skaicius3.TabIndex = 3;
            this.Skaicius3.Text = "3";
            this.Skaicius3.UseVisualStyleBackColor = true;
            this.Skaicius3.Click += new System.EventHandler(this.button3_Click);
            // 
            // Skaicius5
            // 
            this.Skaicius5.Location = new System.Drawing.Point(118, 121);
            this.Skaicius5.Name = "Skaicius5";
            this.Skaicius5.Size = new System.Drawing.Size(39, 23);
            this.Skaicius5.TabIndex = 4;
            this.Skaicius5.Text = "5";
            this.Skaicius5.UseVisualStyleBackColor = true;
            this.Skaicius5.Click += new System.EventHandler(this.button4_Click);
            // 
            // Skaicius4
            // 
            this.Skaicius4.Location = new System.Drawing.Point(50, 121);
            this.Skaicius4.Name = "Skaicius4";
            this.Skaicius4.Size = new System.Drawing.Size(39, 23);
            this.Skaicius4.TabIndex = 5;
            this.Skaicius4.Text = "4";
            this.Skaicius4.UseVisualStyleBackColor = true;
            this.Skaicius4.Click += new System.EventHandler(this.button5_Click);
            // 
            // Skaicius6
            // 
            this.Skaicius6.Location = new System.Drawing.Point(189, 121);
            this.Skaicius6.Name = "Skaicius6";
            this.Skaicius6.Size = new System.Drawing.Size(39, 23);
            this.Skaicius6.TabIndex = 6;
            this.Skaicius6.Text = "6";
            this.Skaicius6.UseVisualStyleBackColor = true;
            this.Skaicius6.Click += new System.EventHandler(this.button6_Click);
            // 
            // Skaicius7
            // 
            this.Skaicius7.Location = new System.Drawing.Point(50, 166);
            this.Skaicius7.Name = "Skaicius7";
            this.Skaicius7.Size = new System.Drawing.Size(39, 23);
            this.Skaicius7.TabIndex = 7;
            this.Skaicius7.Text = "7";
            this.Skaicius7.UseVisualStyleBackColor = true;
            this.Skaicius7.Click += new System.EventHandler(this.button7_Click);
            // 
            // Skaicius8
            // 
            this.Skaicius8.Location = new System.Drawing.Point(118, 166);
            this.Skaicius8.Name = "Skaicius8";
            this.Skaicius8.Size = new System.Drawing.Size(39, 23);
            this.Skaicius8.TabIndex = 8;
            this.Skaicius8.Text = "8";
            this.Skaicius8.UseVisualStyleBackColor = true;
            this.Skaicius8.Click += new System.EventHandler(this.button8_Click);
            // 
            // Skaicius9
            // 
            this.Skaicius9.Location = new System.Drawing.Point(189, 166);
            this.Skaicius9.Name = "Skaicius9";
            this.Skaicius9.Size = new System.Drawing.Size(39, 23);
            this.Skaicius9.TabIndex = 9;
            this.Skaicius9.Text = "9";
            this.Skaicius9.UseVisualStyleBackColor = true;
            this.Skaicius9.Click += new System.EventHandler(this.button9_Click);
            // 
            // zenklas4
            // 
            this.zenklas4.Location = new System.Drawing.Point(50, 205);
            this.zenklas4.Name = "zenklas4";
            this.zenklas4.Size = new System.Drawing.Size(39, 23);
            this.zenklas4.TabIndex = 10;
            this.zenklas4.Text = "*";
            this.zenklas4.UseVisualStyleBackColor = true;
            this.zenklas4.Click += new System.EventHandler(this.button10_Click);
            // 
            // Skaicius0
            // 
            this.Skaicius0.Location = new System.Drawing.Point(118, 205);
            this.Skaicius0.Name = "Skaicius0";
            this.Skaicius0.Size = new System.Drawing.Size(39, 23);
            this.Skaicius0.TabIndex = 11;
            this.Skaicius0.Text = "0";
            this.Skaicius0.UseVisualStyleBackColor = true;
            this.Skaicius0.Click += new System.EventHandler(this.button11_Click);
            // 
            // zenklas5
            // 
            this.zenklas5.Location = new System.Drawing.Point(189, 205);
            this.zenklas5.Name = "zenklas5";
            this.zenklas5.Size = new System.Drawing.Size(39, 23);
            this.zenklas5.TabIndex = 12;
            this.zenklas5.Text = "=";
            this.zenklas5.UseVisualStyleBackColor = true;
            this.zenklas5.Click += new System.EventHandler(this.button12_Click);
            // 
            // zenklas1
            // 
            this.zenklas1.Location = new System.Drawing.Point(234, 95);
            this.zenklas1.Name = "zenklas1";
            this.zenklas1.Size = new System.Drawing.Size(39, 23);
            this.zenklas1.TabIndex = 13;
            this.zenklas1.Text = "+";
            this.zenklas1.UseVisualStyleBackColor = true;
            this.zenklas1.Click += new System.EventHandler(this.button13_Click);
            // 
            // zenklas2
            // 
            this.zenklas2.Location = new System.Drawing.Point(233, 145);
            this.zenklas2.Name = "zenklas2";
            this.zenklas2.Size = new System.Drawing.Size(39, 23);
            this.zenklas2.TabIndex = 14;
            this.zenklas2.Text = "-";
            this.zenklas2.UseVisualStyleBackColor = true;
            this.zenklas2.Click += new System.EventHandler(this.button14_Click);
            // 
            // zenklas3
            // 
            this.zenklas3.Location = new System.Drawing.Point(234, 190);
            this.zenklas3.Name = "zenklas3";
            this.zenklas3.Size = new System.Drawing.Size(39, 23);
            this.zenklas3.TabIndex = 15;
            this.zenklas3.Text = "/";
            this.zenklas3.UseVisualStyleBackColor = true;
            this.zenklas3.Click += new System.EventHandler(this.button15_Click);
            // 
            // button1
            // 
            this.button1.Location = new System.Drawing.Point(50, 234);
            this.button1.Name = "button1";
            this.button1.Size = new System.Drawing.Size(39, 23);
            this.button1.TabIndex = 16;
            this.button1.Text = "←";
            this.button1.UseVisualStyleBackColor = true;
            this.button1.Click += new System.EventHandler(this.button1_Click_1);
            // 
            // button2
            // 
            this.button2.Location = new System.Drawing.Point(118, 234);
            this.button2.Name = "button2";
            this.button2.Size = new System.Drawing.Size(39, 23);
            this.button2.TabIndex = 17;
            this.button2.Text = "C";
            this.button2.UseVisualStyleBackColor = true;
            this.button2.Click += new System.EventHandler(this.button2_Click_1);
            // 
            // button3
            // 
            this.button3.Location = new System.Drawing.Point(189, 234);
            this.button3.Name = "button3";
            this.button3.Size = new System.Drawing.Size(39, 23);
            this.button3.TabIndex = 18;
            this.button3.Text = "2";
            this.button3.UseVisualStyleBackColor = true;
            // 
            // taskas
            // 
            this.taskas.Location = new System.Drawing.Point(234, 227);
            this.taskas.Name = "taskas";
            this.taskas.Size = new System.Drawing.Size(39, 23);
            this.taskas.TabIndex = 19;
            this.taskas.Text = ".";
            this.taskas.UseVisualStyleBackColor = true;
            this.taskas.Click += new System.EventHandler(this.taskas_Click);
            // 
            // Form1
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(284, 262);
            this.Controls.Add(this.taskas);
            this.Controls.Add(this.button3);
            this.Controls.Add(this.button2);
            this.Controls.Add(this.button1);
            this.Controls.Add(this.zenklas3);
            this.Controls.Add(this.zenklas2);
            this.Controls.Add(this.zenklas1);
            this.Controls.Add(this.zenklas5);
            this.Controls.Add(this.Skaicius0);
            this.Controls.Add(this.zenklas4);
            this.Controls.Add(this.Skaicius9);
            this.Controls.Add(this.Skaicius8);
            this.Controls.Add(this.Skaicius7);
            this.Controls.Add(this.Skaicius6);
            this.Controls.Add(this.Skaicius4);
            this.Controls.Add(this.Skaicius5);
            this.Controls.Add(this.Skaicius3);
            this.Controls.Add(this.Skaicius2);
            this.Controls.Add(this.Skaicius1);
            this.Controls.Add(this.Ekranas);
            this.Name = "Form1";
            this.Text = "Form1";
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.TextBox Ekranas;
        private System.Windows.Forms.Button Skaicius1;
        private System.Windows.Forms.Button Skaicius2;
        private System.Windows.Forms.Button Skaicius3;
        private System.Windows.Forms.Button Skaicius5;
        private System.Windows.Forms.Button Skaicius4;
        private System.Windows.Forms.Button Skaicius6;
        private System.Windows.Forms.Button Skaicius7;
        private System.Windows.Forms.Button Skaicius8;
        private System.Windows.Forms.Button Skaicius9;
        private System.Windows.Forms.Button zenklas4;
        private System.Windows.Forms.Button Skaicius0;
        private System.Windows.Forms.Button zenklas5;
        private System.Windows.Forms.Button zenklas1;
        private System.Windows.Forms.Button zenklas2;
        private System.Windows.Forms.Button zenklas3;
        private System.Windows.Forms.Button button1;
        private System.Windows.Forms.Button button2;
        private System.Windows.Forms.Button button3;
        private System.Windows.Forms.Button taskas;
    }
}

