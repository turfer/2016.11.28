﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace WindowsFormsApplication1
{
    public partial class Form1 : Form
    {
        public Form1()
        {
            InitializeComponent();
        }

        private void textBox1_TextChanged(object sender, EventArgs e)
        {

        }

        private void button11_Click(object sender, EventArgs e)
        {
            if(Ekranas.Text.Length < 8)
            {
                Ekranas.Text += "0";
            }
            
        }

        private void button15_Click(object sender, EventArgs e)
        {
            Ekranas.Text += "/";
        }

        private void button1_Click(object sender, EventArgs e)
        {
            if (Ekranas.Text.Length < 8)
            {
                Ekranas.Text += "1";
            }
        }

        private void button2_Click(object sender, EventArgs e)
        {
            if (Ekranas.Text.Length < 8)
            {
                Ekranas.Text += "2";
            }
        }

        private void button3_Click(object sender, EventArgs e)
        {
            if (Ekranas.Text.Length < 8)
            {
                Ekranas.Text += "3";
            }
        }

        private void button4_Click(object sender, EventArgs e)
        {
            if (Ekranas.Text.Length < 8)
            {
                Ekranas.Text += "4";
            }
        }

        private void button5_Click(object sender, EventArgs e)
        {
            if (Ekranas.Text.Length < 8)
            {
                Ekranas.Text += "5";
            }
        }

        private void button6_Click(object sender, EventArgs e)
        {
            if (Ekranas.Text.Length < 8)
            {
                Ekranas.Text += "6";
            }
        }

        private void button7_Click(object sender, EventArgs e)
        {
            if (Ekranas.Text.Length < 8)
            {
                Ekranas.Text += "7";
            }
        }

        private void button8_Click(object sender, EventArgs e)
        {
            if (Ekranas.Text.Length < 8)
            {
                Ekranas.Text += "8";
            }
        }

        private void button9_Click(object sender, EventArgs e)
        {
            if (Ekranas.Text.Length < 8)
            {
                Ekranas.Text += "9";
            }
        }

        private void button10_Click(object sender, EventArgs e)
        {
            Ekranas.Text += "*";
        }

        private void button12_Click(object sender, EventArgs e)
        {
            Ekranas.Text += "=";
        }

        private void button13_Click(object sender, EventArgs e)
        {
            Ekranas.Text += "+";
        }

        private void button14_Click(object sender, EventArgs e)
        {
            Ekranas.Text += "-";
        }

        private void button1_Click_1(object sender, EventArgs e)
        {
            double skaicius = Convert.ToDouble(Ekranas.Text);
            double padalintas = skaicius / 10;
            if (padalintas ==0)
            {
                Ekranas.Text = "";
            }
            else
            {
                Ekranas.Text = padalintas.ToString();
            }
            
        }

        private void button2_Click_1(object sender, EventArgs e)
        {
            Ekranas.Text = " ";
        }

        private void taskas_Click(object sender, EventArgs e)
        {
            if (Ekranas.Text.Length ==  0)
            {
                Ekranas.Text += "0.";
                return;
            }
            if (Ekranas.Text.Length > 6)
            {
                return;
            }
            int count = Ekranas.Text.Split('.').Length - 1;
            if (count > 0)
            {
                return;

            }
            Ekranas.Text += ".";
        }
    }
}
